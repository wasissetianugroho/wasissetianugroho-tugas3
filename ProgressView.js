import {Text, View,StyleSheet} from 'react-native';
import React, {Component} from 'react';
import {ProgressView} from '@react-native-community/progress-view';

const ProgressViewScreen = ({navigation}) => {
  return (
    <View style={styles.SafeAreaStyle}>
        <Text style={styles.TextTitleStyle}>ProgressViewScreen</Text>
        <ProgressView
          style={{width: '100%'}}
          progressTintColor="grenn"
          trackTintColor="black"
          progress={0.6}
          progressViewStyle="default"
        />
        <Text style={styles.TextTitleStyle}>ProgressViewScreen</Text>
      </View>
  );
};
const styles = StyleSheet.create({
  SafeAreaStyle: {
    padding: 24,
    flex: 1,
  },
 
  TextTitleStyle: {
    fontSize: 24,
    marginVertical: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  ButtonContentStyle: {
    backgroundColor: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export default ProgressViewScreen;

