import { View, Text,StyleSheet } from 'react-native';
import React,{ Component, useState } from 'react';
import Slider from '@react-native-community/slider'

const SliderScreen = ({navigation}) => {
    const [sliderValue,setSliderValue] = useState (0);
    const changeSliderValue = value => {
      setSliderValue(value);  
    };
  return (
    <View>
      <Text>Slider Screen</Text>
      <Text>value: {sliderValue}</Text>
      <Slider
      style={{width: 200, height: 40}}
          minimumValue={0}
          maximumValue={100}
          step={1}
          minimumTrackTintColor="red"
          thumbTintColor="black"
          maximumTrackTintColor="blue"
          onValueChange={value => changeSliderValue(value)}
        />
    </View>
  );
};
const styles = StyleSheet.create({

  TextTitleStyle: {
    fontSize: 24,
    marginVertical: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  ButtonContentStyle: {
    backgroundColor: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export default SliderScreen;