
import React, { Component, useEffect, useState } from 'react'
import { 
    Text,
    StyleSheet, 
    View,
    TouchableOpacity,
 } from 'react-native'

 import NetInfo from '@react-native-community/netinfo';

 const NetInfoLibrary= ({navigation}) => {

  const [isConnected, setIsConnected] = useState('');
  const [ipAddress, setIpAddress] = useState('');

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(async (state) => {
      setIsConnected(await state.isConnected);
      setIpAddress(await state.ipAddress);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <View>
      <Text>Connection Status: {isConnected === null ? 'Loading...' : isConnected ? 'Online' : 'Offline'}</Text>
      <Text>{NetInfo.useNetInfo().type }</Text>
      <Text>{ipAddress}</Text>
    </View>
  );
};

const styles = StyleSheet.create({})

  export default NetInfoLibrary;

