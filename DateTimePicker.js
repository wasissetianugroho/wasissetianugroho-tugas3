import { View, Text,Button } from 'react-native'
import React, {Component,useState} from 'react'
import RNDateTimePicker, {
    DateTimePickerAndroid,
  } from '@react-native-community/datetimepicker';  

const DateTimePickerX = ({navigation}) => {
    const [isPickerShow, setIsPickerShow] = useState(false);
    const [date, setDate] = useState(new Date(Date.now()));

    const updateTime = () => {
        setDate (new Date(Date.now()));
    };
    
  return (
    <View>
    {/* <Text>{date.toUTCString()}</Text> */}
        <Button title="Update Time" onPress={updateTime} />
        <Button title="Show Calendar" onPress={() => setIsPickerShow(true)}/>
    {isPickerShow ? (
        <RNDateTimePicker onChange={
            (selected) => {
                setIsPickerShow(false);
                setDate(selected);
            }}
        display='calendar'
        mode='date'
        value= {new Date()}/>
  ) : null}
  </View>
  );
};

export default DateTimePickerX;