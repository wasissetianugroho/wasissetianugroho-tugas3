import React from 'react';
import NetInfo from "./NetInfo";
import App from './App';
import DateTimePickerX from './DateTimePicker';
import SliderScreen from './Slider';
import GeoScreen from './Geolocation';
import ProgressViewScreen from './ProgressView';
import ProgressBarAndroidScreen from './ProgressBarAndroid';
import ClipboardScreen from './Clipboard';
import AsyncScreen from './AsyncStorage';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack= createStackNavigator();

const AppNavigator = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}initialRouteName="App">
            <Stack.Screen name="App" component={App}/>
            <Stack.Screen name="NetInfo" component={NetInfo}/>
            <Stack.Screen name="DateTimePicker" component={DateTimePickerX}/>
            <Stack.Screen name="Slider" component={SliderScreen}/>
            <Stack.Screen name="Geolocation" component={GeoScreen}/>
            <Stack.Screen name="ProgressView" component={ProgressViewScreen}/>
            <Stack.Screen name="ProgressBarAndroid" component={ProgressBarAndroidScreen}/>
            <Stack.Screen name="Clipboard" component={ClipboardScreen}/>
            <Stack.Screen name="AsyncStorage" component={AsyncScreen}/>
            </Stack.Navigator>
            </NavigationContainer>
    )

};
export default AppNavigator;