import {Text, View, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import {ProgressBar} from '@react-native-community/progress-bar-android';

const ProgressBarAndroidScreen = ({navigation}) => {
  return (
    <View style={styles.SafeAreaStyle}>
      <View style={styles.MainContentStyle}>
        <Text style={styles.TextTitleStyle}>ProgressBarAndroid</Text>
        <ProgressBar />
        <ProgressBar styleAttr="Horizontal" />
        <ProgressBar styleAttr="Horizontal" color="#2196F3" />
        <ProgressBar styleAttr="Small" color="#2196F3" />
        <ProgressBar styleAttr="Large" color="#2196F3" />
        <ProgressBar styleAttr="Inverse" color="#2196F3" />
        <ProgressBar styleAttr="SmallInverse" color="#2196F3" />
        <ProgressBar styleAttr="LargeInverse" color="#2196F3" />
        <ProgressBar
          styleAttr="Horizontal"
          indeterminate={false}
          progress={0.5}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
    SafeAreaStyle: {
      padding: 24,
      flex: 1,
    },
    MainContentStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
    },
    TextTitleStyle: {
      fontSize: 24,
      marginVertical: 24,
      fontWeight: 'bold',
      color: 'black',
    },
    ButtonContentStyle: {
      backgroundColor: 'white',
      paddingVertical: 8,
      paddingHorizontal: 16,
      borderRadius: 8,
      marginVertical: 8,
    },
  });  

export default ProgressBarAndroidScreen;

