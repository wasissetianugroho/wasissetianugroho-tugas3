import React from "react";
// import 'react-native-gesture-handler';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from "react-native";

const App = ({navigation}) => {
  return (
    <SafeAreaView style={styles.safeAreaContent}>
      <View style={styles.container}>
        <Text style={styles.label}> React Native Library </Text>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('NetInfo')}>
          <Text style={[{ fontSize: 22 }]}> NetInfo </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('DateTimePicker')}>
          <Text style={[{ fontSize: 22 }]}> DateTimePicker </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('Slider')}>
          <Text style={[{ fontSize: 22 }]}> Slider </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('Geolocation')}>
          <Text style={[{ fontSize: 22 }]}> Geolocation </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('ProgressView')}>
          <Text style={[{ fontSize: 22 }]}> Progress View </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('ProgressBarAndroid')}>
          <Text style={[{ fontSize: 22 }]}> Progress Bar Android </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('Clipboard')}>
          <Text style={[{ fontSize: 22 }]}> Clipboard </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}
        onPress={() => navigation.navigate('AsyncStorage')}>
          <Text style={[{ fontSize: 22 }]}> Async Storage </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Snap Carousel </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Image Zoom Viewer </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Linear Gradient </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Render HTML </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Share </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Skeleton Placeholder </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> WebView </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Rn Tooltip </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonstyle}>
          <Text style={[{ fontSize: 22 }]}> Vector Icons </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  safeAreaContent: {
    backgroundColor: "grey",
    flex: 1,
  },
  label: {
    fontSize: 29,
    fontWeight: "bold",
    paddingStart: 50,
    paddingEnd: 40,
    paddingBottom: 20,
  },
  buttonstyle: {
    backgroundColor: "#fff",
    alignItems: "center",
    marginVertical: 5,
  },
});

export default App;
