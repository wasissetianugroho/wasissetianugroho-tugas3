import {Text, TouchableOpacity, View,StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Clipboard from '@react-native-community/clipboard';
import {TextInput} from 'react-native-gesture-handler';

const ClipboardScreen = ({navigation}) => {
  const [copiedText, setCopiedText] = useState('');
  const [inputText, setInputText] = useState('');

  const copyToClipboard = () => {
    Clipboard.setString('hello world');
  };

  const fetchCopiedText = async () => {
    const text = await Clipboard.getString();
    setCopiedText(text);
  };

  const copyFromTextInput = text => {
    setInputText(text);
  };

  return (
      <View style={styles.MainContentStyle}>
        <TouchableOpacity onPress={copyToClipboard}>
          <Text>Click here to copy to Hello World!</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={fetchCopiedText}>
          <Text>View copied text</Text>
        </TouchableOpacity>

        <Text style={styles.copiedText}>{copiedText}</Text>

        <TextInput
          style={{
            backgroundColor: 'white',
            width: '100%',
            borderRadius: 8,
            marginTop: 16,
          }}
          placeholder="Input text"
          onChangeText={newText => copyFromTextInput(newText)}
        />
        <TouchableOpacity
          onPress={() => {
            setCopiedText(inputText);
            Clipboard.setString(inputText);
          }}
          style={styles.ButtonContentStyle}>
          <Text>Copy</Text>
        </TouchableOpacity>
      </View>
  );
};
const styles= StyleSheet.create({
    MainContentStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
    },
    TextTitleStyle: {
      fontSize: 24,
      marginVertical: 24,
      fontWeight: 'bold',
      color: 'black',
    },
    ButtonContentStyle: {
      backgroundColor: 'white',
      paddingVertical: 8,
      paddingHorizontal: 16,
      borderRadius: 8,
      marginVertical: 8,
    },
  });  

export default ClipboardScreen;
