import {Button, Image, PermissionsAndroid, Text, View,StyleSheet} from 'react-native';
import React, {Component, useEffect, useState} from 'react';
import Geolocation from '@react-native-community/geolocation';
import { Platform } from 'react-native';

const GeoScreen = ({navigation}) => {
  const [locationStatus, setLocationStatus] = useState('');
  const [currentLongitude, setCurrentLongitude] = useState('...');
  const [currentLatitude, setCurrentLatitude] = useState('...');

  var watchID;

  useEffect(() => {
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            setLocationStatus('Permission Denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    };
    requestLocationPermission();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);

  const getOneTimeLocation = () => {
    setLocationStatus('Getting Location ...');
    Geolocation.getCurrentPosition(
      position => {
        setLocationStatus('You are Here');

        const currentLongitude = JSON.stringify(position.coords.longitude);
        const currentLatitude = JSON.stringify(position.coords.latitude);

        setCurrentLongitude(currentLongitude);
        setCurrentLatitude(currentLatitude);
      },
      error => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000,
      },
    );
  };

  const subscribeLocationLocation = () => {
    watchID = Geolocation.watchPosition(
      position => {

        setLocationStatus('You are Here');
        console.log(position);

        const currentLongitude = JSON.stringify(position.coords.longitude);
        const currentLatitude = JSON.stringify(position.coords.latitude);
        setCurrentLongitude(currentLongitude);
        setCurrentLatitude(currentLatitude);
      },
      error => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
      },
    );
  };

  return (
    <View style={[styles.SafeAreaStyle, {backgroundColor: 'white'}]}>
      <View style={styles.MainContentStyle}>
        <Image
          source={{
            uri: 'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
          }}
          style={{width: 100, height: 100}}
        />
        <Text style={styles.boldText}>{locationStatus}</Text>

        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 16,
          }}>
          Longitude: {currentLongitude}
        </Text>

        <Text
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 16,
          }}>
          Latitude: {currentLatitude}
        </Text>

        <View style={{marginTop: 20}}>
          <Button
            title="Refresh Current Position"
            onPress={getOneTimeLocation}
          />
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  SafeAreaStyle: {
    padding: 24,
    flex: 1,
  },
  MainContentStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  TextTitleStyle: {
    fontSize: 24,
    marginVertical: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  ButtonContentStyle: {
    backgroundColor: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export default GeoScreen;

